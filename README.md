# Battleships

## The Task.

Implement a battle ship game that can be played from the
python shell.

Players start a new game then add their ships to a grid.
Players then take turns guessing where their opponents
ships are and shooting there. The players are very
trusting and take turns using a python shell to play
their game.

Ships occupy 3 cells in the grid, cant overlap and must
be fully on the grid. They can be either horizontal or
vertical. When all three cells are hit, the ship is sunk
When all the ships belonging to a player are sunk, that
player is out of the game. The last player still in the
game wins.


## Implementation.

Four functions are defined below, these describe the API
we want to use and the functionality of the game. Implement
these in Python 2.7 or 3.4, let us know which you chose.


## Example working game.

```python
>>> import battleships
>>> battleships.new_game(10, 5)
>>> battleships.place_ship('rob', 1, 0, 'Horizontal')
>>> battleships.place_ship('dave', 7, 3, 'Vertical')
>>> battleships.place_ship('rob', 3, 3, 'Vertical')
>>> battleships.print_grid('rob')
###~~~~~~~()
~~~~~~~~~~()
~~~#~~~~~~()
~~~#~~~~~~()
~~~#~~~~~~()
>>> battleships.print_grid('dave')
~~~~~~~~~~()
~~~~~~~~~~()
~~~~~~~#~~()
~~~~~~~#~~()
~~~~~~~#~~()
>>>
>>> battleships.fire(0, 0)
Hit!
>>> battleships.fire(1, 0)
Hit!
>>> battleships.print_grid('dave')
!!~~~~~~~~()
~~~~~~~~~~()
~~~~~~~#~~()
~~~~~~~#~~()
~~~~~~~#~~()
>>> battleships.fire(2, 0)
Hit!
rob lost a ship!
>>> battleships.fire(3, 0)
Miss
>>> battleships.fire(3, 2)
Hit!
>>> battleships.fire(3, 3)
Hit!
>>> battleships.fire(3, 4)
Hit!
rob lost a ship!
rob is out of the game
dave is the winner!
```

## Boilerplate functions

```python
def new_game(grid_width, grid_height):
    """
        This function takes two integers and creates a new
        grid for us to play on. The arguments are the width
        and height of the grid
    """


def place_ship(player_name, x, y, direction):
    """
        A player places a ship at (x, y) on the grid,
        in direction (either 'Horizontal' or 'Vertical'.
        The ship is 3 cells long and centered on (x, y).

        A Value Error is raise if the ship would overlap
        another ship, or not be fully on the grid.
    """


def fire(x, y):
    """
        Shoot at the cell (x, y).

        If the shot missess, print 'Miss' to stdout

        If the shot hits, print 'Hit!' to stdout

        If all 3 cells of a ship have been hit, the ship is sunk,
        print '<name of player who owns the ship> lost a ship!'

        If a player just lost their last ship, write to
        stout: '<name of player who is out> is out of the game'

        If there is only one player left with any ships,
        print '<name of player left> is the winner!'
    """


def print_grid(player_name):
    """
        Prints a grid representing the grid. The cell (0, 0)
        is in the top left.

        Only show the ships that belong to the player.
        Use the following characters:

        '~' empty water
        '#' players ships
        '.' misses
        '!' hits
    """
```
