"""
'Battleship' test assignment. Developed in Python 2.7.11.
"""


grid = []  # The grid representing the game board, the sea.
players = {}  # A dictionary containing players and their fleets.


class Ship(object):
    """
    A 'ship' in the sea. It holds the complete coordinates of the ship in the
    'sea' grid and the name of the player who owns the ship.
    """
    def __init__(self, player, start_x, start_y, direction, grid):
        self.player = player
        self.direction = direction.lower()

        # The 'coordinates'. A dictionary holding the grid positions occupied by
        # the ship (the keys) and the status of that specific positions (the
        # values).
        self.coordinates = {(start_x, start_y): '#'}

        self.position_ship(start_x, start_y, grid)

    def position_ship(self, start_x, start_y, grid):
        """
        Check the grid for selected positions availability. If it is available,
        place the ship in those positions.
        """
        # Check that the whole ship will be inside of the grid.
        if self.direction == 'horizontal' and start_x + 2 > len(grid[0]) or \
                self.direction == 'vertical' and start_y + 2 > len(grid):
            raise ValueError('Ship is out of the grid')

        # Set all ship coordinates.
        if self.direction == 'vertical':
            self.coordinates.update({(start_x, start_y + 1): '#'})
            self.coordinates.update({(start_x, start_y + 2): '#'})
        else:
            self.coordinates.update({(start_x + 1, start_y): '#'})
            self.coordinates.update({(start_x + 2, start_y): '#'})

        # Check that the ship won't overlap another ship.
        for x, y in self.coordinates.keys():
            if grid[y][x] is not None:
                raise ValueError('Ship is overlapping another ship')

        # Ship can be placed now in the sea, marking the 'points' in the grid
        # occupied by that ship.
        for x, y in self.coordinates.keys():
            grid[y][x] = self


def new_game(grid_width, grid_height):
    """
        This function takes two integers and creates a new
        grid for us to play on. The arguments are the width
        and height of the grid
    """
    global grid

    if len(grid) == 0:
        # Check that the grid is actually empty.
        for row in xrange(grid_height):
            grid.append([None for _ in xrange(grid_width)])
    else:
        print('Grid already created.')


def place_ship(player_name, x, y, direction):
    """
        A player places a ship at (x, y) on the grid,
        in direction (either 'Horizontal' or 'Vertical'.
        The ship is 3 cells long and centered on (x, y).

        A Value Error is raise if the ship would overlap
        another ship, or not be fully on the grid.
    """
    global grid
    global players

    if len(grid) == 0:
        print('We do not have a grid yet. Create a grid to play.')

        return

    # Instantiate a new `Ship` object. If everything goes well, the grid-list
    # will now have the passed coordinates/direction items filled with it.
    ship = Ship(player_name, x, y, direction, grid)

    # `player_name` fleet has an new ship.
    players.setdefault(player_name, []).append(ship)


def fire(x, y):
    """
        Shoot at the cell (x, y).

        If the shot missess, print 'Miss' to stdout

        If the shot hits, print 'Hit!' to stdout

        If all 3 cells of a ship have been hit, the ship is sunk,
        print '<name of player who owns the ship> lost a ship!'

        If a player just lost their last ship, write to
        stout: '<name of player who is out> is out of the game'

        If there is only one player left with any ships,
        print '<name of player left> is the winner!'
    """
    global grid
    global players

    position = grid[y][x]

    if position is None or position.coordinates[(x, y)] == '!':
        # Target missed.
        print('Miss')
    else:
        # Hit the ship.
        position.coordinates[(x, y)] = '!'
        print('Hit!')

        # Check how is the ship doing.
        if '#' not in position.coordinates.values():
            players[position.player].remove(position)
            print('{0} lost a ship!'.format(position.player))

            # Check how the fleet of `position.player` is doing.
            if not players[position.player]:
                players.pop(position.player, None)
                print('{0} is out of the game'.format(position.player))

            # Check general game status for any winner.
            if len(players) == 1:
                print('{0} is the winner!'.format(players.popitem()[0]))


def print_grid(player_name):
    """
        Prints a grid representing the grid. The cell (0, 0)
        is in the top left.

        Only show the ships that belong to the player.
        Use the following characters:

        '~' empty water
        '#' players ships
        '.' misses
        '!' hits
    """
    for y, row in enumerate(grid):
        for x, position in enumerate(row):
            if position is None or position.player != player_name:
                # Water, or ships that `player_name` does not own.
                print('~'),
            else:
                # `player_name` can actually see status of his/her fleet.
                print(position.coordinates[(x, y)]),
        print('')
